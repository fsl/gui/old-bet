import { Box } from "@mui/material"
import {CssBaseline} from "@mui/material"
import {Container} from "@mui/material"
import { FileField } from "@components"
import { Title } from '@components'
import { NumberSlider } from "@components"
import {ActionsPanel} from "@components"
import { CommandPreview } from "@components"
import { OptionsPanel } from "@components"
import { CheckBox } from "@components"
import React from "react"
import { defaultBetOpts } from "./libbet"
import { NiivueDisplay } from "@components"
import { createCommandString } from "@fsljs"
import { createSocketClient } from "@fsljs"
import { getCommsConfig } from "@fsljs"
import { removeExtension } from "@fsljs"
import { getFilenameFromURL } from "@fsljs"
import { webGl2Supported } from "@fsljs"

export function Bet(props) {
  const [optionsPanelVisible, setOptionsPanelVisible] = React.useState(false)
  const [opts, setOpts] = React.useState(defaultBetOpts)
  const [commandText, setCommandText] = React.useState('bet [input] [output] [options]')
  const [socketClient, setSocketClient] = React.useState(null)
  const [runDisabled, setRunDisabled] = React.useState(true)
  const [niivueImages, setNiivueImages] = React.useState([])
  const [fileServerPort, setFileServerPort] = React.useState(0)
  const optsRef = React.useRef(opts)

  function setCommandTextWithPrefix(text, prefix='bet'){
    setCommandText(prefix + ' ' + text)
  }
  // loop over app opts and add react useEffect hooks to each object key
  // this will allow us to update the state of the app when the opts change
  Object.keys(opts).forEach((key)=>{
    React.useEffect(()=>{
      // if input is not different from default, disable the run button
      if (opts['input'] === defaultBetOpts['input']){
        setRunDisabled(true)
      } else {
        setRunDisabled(false)
      }
      createCommandString(opts, defaultBetOpts, (text)=>{setCommandTextWithPrefix(text, 'bet')})
      optsRef.current = opts
    }, [opts[key]])
  })

  React.useEffect(()=>{
    if (opts['input'] !== null){
      let output = removeExtension(opts['input']) + '_brain' + '.nii.gz'
      updateOutputValue(output)
    }
  }, [opts['input']])

  function getFileName(){
    socketClient.emit('file')
  }

  function onFileEvent(file){
    const comms = getCommsConfig()
    updateInputValue(file)
    let url = `http://localhost:${comms.fileServerPort}/file?filename=${file}`
    if (comms.fileServerPort !== 0){
      setNiivueImages([{url: url, 'colorMap': 'gray', 'name': getFilenameFromURL(url)}])
    }
  }

  function onRunEvent(message){
    // don't update niivue images if the message is related to stdout or stderr
    if (message.stdout !== undefined || message.stderr !== undefined){
      return
    }
    setRunDisabled(false)
    const comms = getCommsConfig()
    let files = [
      optsRef.current['input'],
      optsRef.current['output']
    ]
    let urls = []
    for (let i = 0; i < files.length; i++){
      let url = `http://localhost:${comms.fileServerPort}/file?filename=${files[i]}`
      urls.push(url)
    }
    let nvimages = []
    for (let i = 0; i < urls.length; i++){
      nvimages.push({url: urls[i], 'colorMap': i === 0 ? 'gray' : 'red', 'name': getFilenameFromURL(urls[i])})
    }
    if (comms.fileServerPort !== 0){
      setNiivueImages(nvimages)
    }
  }

  function registerSocketListeners(socket){
    socket.on('file', onFileEvent)
    socket.on('run', onRunEvent)
  }


  React.useEffect(()=>{
    const comms = getCommsConfig()
    const socket = createSocketClient(comms.socketServerPort)
    registerSocketListeners(socket)
    setSocketClient(socket)
    setFileServerPort(comms.fileServerPort)
    // when the component unmounts, close the socket
    return () => {
      setSocketClient(null)
    }
  }, [])

  const toggleOptionsPanel = function(){
    setOptionsPanelVisible(!optionsPanelVisible)
  }

  const handleCheckBoxToggle = function(option){
    setOpts({
      ...opts,
      ...{[option]: !opts[option]}
    })
  }

  const updateFValue = function(value){
    setOpts({
      ...opts,
      ...{['-f']: value}
    })
  }

  const updateGValue = function(value){
    setOpts({
      ...opts,
      ...{['-g']: value}
    })
  }

  const updateInputValue = function(value){
    setOpts({
      ...opts,
      ...{['input']: value}
    })
  }

  const updateOutputValue = function(value){
    setOpts({
      ...opts,
      ...{['output']: value}
    })
  }

  function onRun(){
    socketClient.emit('run', commandText)
    setRunDisabled(true)
  }

  return (
    <Container
      component='main'
      style={{height: '100%'}}
    >
      <CssBaseline></CssBaseline>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          height: '100%',
          width: '100%'
        }}
      >
        <Title text='Bet' />
        
        <FileField 
          label='Input file'
          value={opts['input']}
          onInput={updateInputValue}
          onClick={getFileName}
        />

        <FileField 
          label='Output file' 
          value={opts['output']}
          onInput={updateOutputValue}
          showChooseButton={false}
        />

        <NumberSlider 
          label='f-value'
          value={opts['-f']}
          min={0.0}
          max={1.0}
          step={0.01}
          onInput={updateFValue}
        />
        <ActionsPanel 
          onRun={onRun}
          onMoreOptions={toggleOptionsPanel}
          runDisabled={runDisabled}
        />

        <CommandPreview text={commandText} />

        <OptionsPanel visible={optionsPanelVisible}>
          <NumberSlider 
            label='g-value'
            value={opts['-g']}
            min={-1.0}
            max={1.0}
            step={0.01}
            onInput={updateGValue}
          />

          <CheckBox
            label='Create brain outline image'
            onChange={()=>{handleCheckBoxToggle('-o')}}
            checked={opts['-o']}
          />
          <CheckBox
            label='Create binary brain mask'
            onChange={()=>{handleCheckBoxToggle('-m')}}
            checked={opts['-m']}
          />
          <CheckBox
            label='Output approximate skull image'
            onChange={()=>{handleCheckBoxToggle('-s')}}
            checked={opts['-s']}
          />
          <CheckBox
            label='Do not create final brain image'
            onChange={()=>{handleCheckBoxToggle('-n')}}
            checked={opts['-n']}
          />
          <CheckBox
            label='Apply thresholding to brain and mask'
            onChange={()=>{handleCheckBoxToggle('-t')}}
            checked={opts['-t']}
          />
          <CheckBox
            label='Save brain surface mesh'
            onChange={()=>{handleCheckBoxToggle('-e')}}
            checked={opts['-e']}
          />
          <CheckBox
            label='Switch on diagnostic messages'
            onChange={()=>{handleCheckBoxToggle('-v')}}
            checked={opts['-v']}
          />
          <CheckBox
            label='Debug: do not delete intermediate images'
            onChange={()=>{handleCheckBoxToggle('-d')}}
            checked={opts['-d']}
          />

        </OptionsPanel>
        {/* if webGl2 supported */}
        { webGl2Supported() && <NiivueDisplay height={600} images={niivueImages}/>}
        
    </Box>

    </Container> 
  )
}

