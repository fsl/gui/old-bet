export const defaultBetOpts = {
    'input':        'input.nii.gz',    // input file path
    'output':       'output.nii.gz',     // output file path
    '-o':           false,
    '-m':           false,
    '-s':           false,
    '-n':           false,
    '-f':           0.5,
    '-g':           0,
    '-r':           null,
    '-c':           null,
    '-t':           false,
    '-e':           false,
    '-R':           null,
    '-S':           null,
    '-B':           null,
    '-Z':           null,
    '-F':           null,
    '-A':           null,
    '-A2':          null,
    '-v':           false,
    '-d':           false
}


